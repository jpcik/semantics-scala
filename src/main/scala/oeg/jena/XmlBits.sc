package oeg.jena

import scala.xml._

object XmlBits {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val xml =
    <people>
      <person name="Waldo" age="19"/>
      <person name="Bart" age="6"/>
      <person lastname="Rafa" age="53"/>
      <person name="Bety" age="3"/>
     </people>                                    //> xml  : scala.xml.Elem = <people>
                                                  //|       <person name="Waldo" age="19"/>
                                                  //|       <person name="Bart" age="6"/>
                                                  //|       <person lastname="Rafa" age="53"/>
                                                  //|       <person name="Bety" age="3"/>
                                                  //|      </people>
  (xml \ "person").map(elem=>elem.attribute("name"))
                                                  //> res0: scala.collection.immutable.Seq[Option[Seq[scala.xml.Node]]] = List(Som
                                                  //| e(Waldo), Some(Bart), None, Some(Bety))
  
}