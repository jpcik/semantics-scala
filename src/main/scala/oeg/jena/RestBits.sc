package oeg.jena

//import play.api.libs.ws._
import dispatch._
import xml._

object RestBits {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val oops=url("http://oops-ws.oeg-upm.net/rest").POST
                                                  //> oops  : com.ning.http.client.RequestBuilder = com.ning.http.client.RequestBu
                                                  //| ilder@60fba110
  val req=
    <OOPSRequest>
      <OntologyUrl>http://www.cc.uah.es/ie/ont/learning-resources.owl</OntologyUrl>
      <OntologyContent></OntologyContent>
      <Pitfalls>10</Pitfalls>
      <OutputFormat>XML</OutputFormat>
    </OOPSRequest>                                //> req  : scala.xml.Elem = <OOPSRequest>
                                                  //|       <OntologyUrl>http://www.cc.uah.es/ie/ont/learning-resources.owl</Ontol
                                                  //| ogyUrl>
                                                  //|       <OntologyContent></OntologyContent>
                                                  //|       <Pitfalls>10</Pitfalls>
                                                  //|       <OutputFormat>XML</OutputFormat>
                                                  //|     </OOPSRequest>
  oops.setBody(req.toString)                      //> res0: com.ning.http.client.RequestBuilder = com.ning.http.client.RequestBuil
                                                  //| der@60fba110
  val rs= Http(oops OK as.xml.Elem)               //> rs  : dispatch.Promise[scala.xml.Elem] = Promise(-incomplete-)
  
  println(rs())                                   //> <oops:OOPSResponse xmlns:oops="http://www.oeg-upm.net/oops">
                                                  //| <oops:Pitfall>
                                                  //| <oops:Code>P10</oops:Code><oops:Name>Missing disjointness [1, 2, 3]</oops:Na
                                                  //| me>
                                                  //| <oops:Description>The ontology lacks disjoint axioms between classes or betw
                                                  //| een properties that should be defined as disjoint. For example, we can creat
                                                  //| e the classes “Odd” and “Even” (or the classes “Prime” and “Co
                                                  //| mposite”) without being disjoint; such representation is not correct based
                                                  //|  on the definition of these types of numbers.</oops:Description>
                                                  //| </oops:Pitfall>
                                                  //| </oops:OOPSResponse>
  
}