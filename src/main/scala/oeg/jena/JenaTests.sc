package oeg.jena

import com.hp.hpl.jena.rdf.model.ModelFactory
import com.hp.hpl.jena.rdf.model.ResourceFactory
import com.hp.hpl.jena.vocabulary._
import com.hp.hpl.jena.sparql.vocabulary.FOAF

object JenaBits {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val people=Seq("Coco","Pili","Nick","Rita","Jeni")
                                                  //> people  : Seq[String] = List(Coco, Pili, Nick, Rita, Jeni)
  val prefix="http://example.org/people/"         //> prefix  : String = http://example.org/people/
      
  val puris=people.map(p=>prefix+p)               //> puris  : Seq[String] = List(http://example.org/people/Coco, http://example.o
                                                  //| rg/people/Pili, http://example.org/people/Nick, http://example.org/people/Ri
                                                  //| ta, http://example.org/people/Jeni)
  
  
  val uris=people.map(p=>ResourceFactory.createResource(prefix+p))
                                                  //> uris  : Seq[com.hp.hpl.jena.rdf.model.Resource] = List(http://example.org/pe
                                                  //| ople/Coco, http://example.org/people/Pili, http://example.org/people/Nick, h
                                                  //| ttp://example.org/people/Rita, http://example.org/people/Jeni)


  val model=ModelFactory.createDefaultModel       //> model  : com.hp.hpl.jena.rdf.model.Model = <ModelCom   {} | >

  uris.foreach{uri=>
    model.add(uri,RDF.`type`,FOAF.Person)
  }
  model.write(System.out,"TTL")                   //> <http://example.org/people/Nick>
                                                  //|         a       <http://xmlns.com/foaf/0.1/Person> .
                                                  //| 
                                                  //| <http://example.org/people/Coco>
                                                  //|         a       <http://xmlns.com/foaf/0.1/Person> .
                                                  //| 
                                                  //| <http://example.org/people/Pili>
                                                  //|         a       <http://xmlns.com/foaf/0.1/Person> .
                                                  //| 
                                                  //| <http://example.org/people/Rita>
                                                  //|         a       <http://xmlns.com/foaf/0.1/Person> .
                                                  //| 
                                                  //| <http://example.org/people/Jeni>
                                                  //|         a       <http://xmlns.com/foaf/0.1/Person> .
                                                  //| res0: com.hp.hpl.jena.rdf.model.Model = <ModelCom   {http://example.org/peop
                                                  //| le/Nick @http://www.w3.org/1999/02/22-rdf-syntax-ns#type http://xmlns.com/fo
                                                  //| af/0.1/Person; http://example.org/people/Coco @http://www.w3.org/1999/02/22-
                                                  //| rdf-syntax-ns#type http://xmlns.com/foaf/0.1/Person; http://example.org/peop
                                                  //| le/Pili @http://www.w3.org/1999/02/22-rdf-syntax-ns#type http://xmlns.com/fo
                                                  //| af/0.1/Person; http://example.org/people/Rita @http://www.w3.org/1999/02/22-
                                                  //| rdf-syntax-ns#type h
                                                  //| Output exceeds cutoff limit.
}