package oeg.jena

import com.hp.hpl.jena.rdf.model.Model
import com.hp.hpl.jena.rdf.model.ModelFactory
import collection.JavaConversions._
import com.hp.hpl.jena.vocabulary.RDF
import com.hp.hpl.jena.sparql.vocabulary.FOAF

object ScalaJena {
  def readRdf={
    
    val m=ModelFactory.createDefaultModel
    m.read("people.ttl", "TTL")
    m
  }
  
  def main(args:Array[String]):Unit={
    val sts=readRdf.listStatements.toSeq
    //sts.foreach(st=>println(st))
    
    sts.filter(st=>st.getSubject.getURI.endsWith("tony"))
       .foreach(println)
       
    sts.filter(st=>st.getPredicate.getURI.endsWith("country"))
       .foreach(println)
       
    sts.filter(st=>st.getPredicate.getURI.endsWith("country"))
       .map(st=>st.getObject.asResource.getLocalName)  
       .foreach(println)
    
    
  }
}