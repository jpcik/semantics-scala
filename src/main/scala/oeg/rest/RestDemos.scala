package oeg.rest

//import play.api.libs.ws.WS
import dispatch._

object RestDemos {
  
  val s4tags=url("http://grafias.dia.fi.upm.es/SemanticTagWebServiceRestFul/resources/tag/disambiguate")
  //val s4tags=url("http://gsn.linkeddata.es/multidata")
  s4tags.addQueryParameter("tag","Estaci%C3%B3n") 
  s4tags.addQueryParameter("lang","es") 
  s4tags.addQueryParameter("contextstr","tren,%20transporte") 
    
  def callRest={
   //WS.url(s4tags).get
    val pp=Http(s4tags OK as.xml.Elem)
    pp()
  }
  
  def main(args:Array[String]):Unit={
    println("copas")
    val res=RestDemos.callRest
    res \\ "URI" map {elem=>
      println(elem)
    }
    
    
  }
}


