object BasicScala {

  var name:String="something"                     //> name  : String = something

  var i =4                                        //> i  : Int = 4

   val name2 ="xx"                                //> name2  : String = xx
   
   name="dfsdf"

  val list=List(2,3,4,5,6)                        //> list  : List[Int] = List(2, 3, 4, 5, 6)
  val list2 = List("a","b","c")                   //> list2  : List[String] = List(a, b, c)
  
  
  list.filter{i=>
    i<3
 
  }                                               //> res0: List[Int] = List(2)
  
  list2.foreach{str=>
    println(str)
  }                                               //> a
                                                  //| b
                                                  //| c
  

  list.map(term=>term*2)                          //> res1: List[Int] = List(4, 6, 8, 10, 12)

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val a=1                                         //> a  : Int = 1
  a+2                                             //> res2: Int = 3
  val b:Int=3                                     //> b  : Int = 3
  
}