name := "semantics-scala"

organization := "es.upm.fi.oeg"

version := "0.0.1"

scalaVersion := "2.10.3"

crossPaths := false

libraryDependencies ++= Seq(
  "net.databinder.dispatch" %% "dispatch-core" % "0.9.5",
  "org.apache.jena" % "jena-core" % "2.11.0" intransitive,
  "org.apache.jena" % "jena-arq" % "2.11.0" intransitive,
  "org.apache.jena" % "jena-iri" % "1.0.0" intransitive,
  "xerces" % "xercesImpl" % "2.11.0" ,
  "com.typesafe.play" %% "play" % "2.2.1" intransitive,
  "com.typesafe.play" %% "play-json" % "2.2.1" intransitive,
  "com.typesafe.play" %% "play-iteratees" % "2.2.1" intransitive,
  "com.typesafe.play" %% "templates" % "2.2.1" intransitive,
  "com.fasterxml.jackson.core" % "jackson-core" % "2.2.2" intransitive,
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.2.2" intransitive,   
  "com.fasterxml.jackson.core" % "jackson-annotations" % "2.2.2" intransitive,   
  "ch.qos.logback" % "logback-classic" % "1.0.13",  
  "org.scalatest" % "scalatest_2.10" % "2.0.RC1" % "test"
)

resolvers ++= Seq(
  DefaultMavenRepository 
)

publishTo := Some("Artifactory Realm" at "http://aldebaran.dia.fi.upm.es/artifactory/sstreams-releases-local")
    
credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
    
publishMavenStyle := true
    
publishArtifact in (Compile, packageSrc) := false 

scalacOptions += "-deprecation"

