package controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._

object BigService extends Controller{

  def bestMethod = Action {
    Ok("best string in the world")
  }

  def complicated = Action{
    //some work
    val comp= new ComplicatedProcessor
    Ok(comp.veryComplexJson)
  }
  
}

class ComplicatedProcessor {
  
  def veryComplexJson={
    
   Json.obj(
      "users" -> Json.arr(
        Json.obj("name" -> "Bob","age" -> 31,"email" -> "bob@gmail.com"),
        Json.obj("name" -> "Kiki","age" -> 25,"email" -> JsNull)
      ))
  }
}